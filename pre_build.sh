#!/bin/bash

sudo apt-get install -y \
    libboost-all-dev \
    libgoogle-glog-dev \
    libnetfilter-conntrack-dev \
    libnetfilter-queue-dev
