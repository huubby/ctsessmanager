default: debug

.PHONY: debug release clean
#.PHONY: debug release test clean

release depend ::
	$(MAKE) -C src -f release.mk $@

debug depend ::
	$(MAKE) -C src -f debug.mk $@

test::
	$(MAKE) -C src -f debug.mk lib
	$(MAKE) -C unittest $@

greek::
	$(MAKE) -C src -f debug.mk lib
	$(MAKE) -C unittest $@

clean::
	$(MAKE) -C src $@

#clean::
#	$(MAKE) -C src $@
#	$(MAKE) -C unittest $@
