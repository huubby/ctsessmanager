#ifndef __COMMON_H__
#define __COMMON_H__

#define PROGRAM_NAME    "Conntrack Session Manager (csm)"
#define MAJOR_VERSION   1
#define MINOR_VERSION   0
#define BUILD_NO        2

#define KB(x) ((uint64_t) (x) << 10)
#define MB(x) ((uint64_t) (x) << 20)
#define GB(x) ((uint64_t) (x) << 30)
#define TB(x) ((uint64_t) (x) << 40)

#define VLOG_DEFAULT  1
#define VLOG_NOTICE   2
#define VLOG_DEBUG    3
#define VLOG_TRACE    4


#endif // __COMMON_H__
