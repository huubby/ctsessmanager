#ifndef __CT_SESS_MANAGER_H__
#define __CT_SESS_MANAGER_H__

#include <stdint.h>
#include <functional>
#include <unordered_set>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <boost/pool/object_pool.hpp>
#include <boost/circular_buffer.hpp>
#include <libnetfilter_conntrack/libnetfilter_conntrack.h>
#include "Exception.h"

DEFINE_EXCEPTION(StartThreadFailed);
DEFINE_EXCEPTION(OpenCTNetlink);
DEFINE_EXCEPTION(CreateDumpFilter);
DEFINE_EXCEPTION(RegisterCallback);
DEFINE_EXCEPTION(CatchCTEvent);
DEFINE_EXCEPTION(DumpQuery);
DEFINE_EXCEPTION(InvalidIPAddr);

struct CtSession
{
    uint64_t bytes;
    uint32_t src;
    uint32_t dst;
    uint16_t sport;
    uint16_t dport;
    uint8_t proto;

    nf_conntrack_msg_type type;

    bool operator==(const CtSession& other) const;
};

struct CtSessPtrEqual
{
    bool operator()(const CtSession* s1, const CtSession* s2) const;
};

namespace std {
    template<>
        struct hash<CtSession>
        {
            std::size_t operator()(const CtSession& s) const
            {
                return ((hash<unsigned int>()(s.src)
                            ^ (hash<unsigned int>()(s.dst) << 1)) >> 1)
                    ^ ((hash<unsigned short>()(s.sport)
                                ^ (hash<unsigned short>()(s.dport) << 1)) >> 1);
            }
        };

    template<>
        struct hash<CtSession*>
        {
            std::size_t operator()(const CtSession* sp) const
            {
                return hash<CtSession>()(*sp);
            }
        };
}

class CtSessManager
{
    typedef std::unordered_set<CtSession*,
        std::hash<CtSession*>, CtSessPtrEqual> CtSessList;

public:
    CtSessManager(uint64_t threshold, int facility, int severity);
    ~CtSessManager();

    void Run();
    void Stop();
    bool IsRunning() const { return m_running; }
    void Enqueue(nf_conntrack_msg_type type, const nf_conntrack *ct);

private:
    void PrepareStat();
    void GetStat();
    void TeardownStat();

    void SetupListen();
    void ListenEvents(bool &idle);
    void TeardownListen();

    void Process();

    bool ShouldReport(const CtSession* sess, bool destroyed);
    void SendSyslog(const CtSession* sess);
    std::string IPv4Str(uint32_t ip);
    std::string ProtoStr(uint8_t proto);

private:
    bool m_running;
    uint64_t m_threshold;
    int m_facility;
    int m_severity;

    CtSessList m_sessions;
    boost::object_pool<CtSession> m_pool;

    nfct_handle *m_statNfctHandle;

    nfct_handle *m_evtNfctHandle;
    int m_evtNfctFd;

    std::shared_ptr<std::thread> m_processThr;
    std::mutex m_queueMutex;
    std::condition_variable m_queueCondition;
    boost::circular_buffer<CtSession> m_sessQueue;
};



#endif // __CT_SESS_MANAGER_H__
