#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <signal.h>
#include <vector>
#include <algorithm>
#include <string>
#include <glog/logging.h>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include "CtSessManager.h"
#include "common.h"

#define SYSLOG_NAMES
#include "syslog.h"

static std::string log_folder("log");
static int log_stderrthreshold=0;
static int log_logbufsecs=5;
static int log_minloglevel=0;
static int log_stop_logging_if_full_disk=1;
static int log_max_log_size=1;
static int g_log_v;

static uint64_t g_threshold = 0;
static const char* g_syslog_facility_name = "user";
static int g_syslog_facility;
static const char* g_syslog_severity_name = "notice";
static int g_syslog_severity;
static CtSessManager *g_ct_sess_manager = nullptr;

void CtrlHandler(int sig)
{
    if (g_ct_sess_manager && g_ct_sess_manager->IsRunning())
        g_ct_sess_manager->Stop();

    switch (sig)
    {
        // SIGINT is sent when the user presses the CTRL-C keys
        case SIGTERM:
        case SIGINT:
            // Set the Ctrl Handler
            signal(sig, CtrlHandler);
            return;

        // SIGHUP is intended to be sent from some arbitrary external process,
        // including administrative scripts, the administrative web GUI, etc.
        case SIGHUP:
            // Set the Ctrl Handler
            signal(SIGHUP, CtrlHandler);
            return;

        case SIGUSR1:
        case SIGUSR2:
            signal(sig, CtrlHandler);
            return;
    }
}

#define VALID_UNIT(u) \
    (((u) > 47 && (u) < 58) || \
     (u) == 'k' || (u) == 'm' || (u) == 'g' || (u) == 't')
void parse_options(int argc, char *argv[])
{
    namespace po = boost::program_options;
    po::options_description desc("Options");
    desc.add_options()
        ("help,h", "Show this messages")

        ("threshold,t", po::value<std::string>()->required(),
         "Reporting threshold value, integer with suffixes (case incensitive) "
         "like 'k','m','g','t', bare number means bytes.")

        ("facility,f",
         po::value<std::string>()->default_value("user"),
         "Syslog facility that reporting message will be sending with.")

        ("severity,s",
         po::value<std::string>()->default_value("notice"),
         "Syslog severity that reporting message will be sending with.");

    po::options_description realDesc(desc);
    realDesc.add_options()
        ("verbose,v", po::value<int>(&g_log_v)->default_value(VLOG_DEFAULT),
         "Verbose logging level");

    po::variables_map vm;
    try {
        po::store(po::parse_command_line(argc, argv, realDesc), vm);
        po::notify(vm);

        if (vm.count("help")) {
            std::cout << desc << std::endl;
            exit(0);
        }

        if (vm.count("threshold")) {
            std::string val = vm["threshold"].as<std::string>();
            char unit = ::tolower(val.back());
            int n = std::stoi(val);
            if (!VALID_UNIT(unit) || n < 0) {
                std::cout << desc << std::endl;
                exit(0);
            }
            switch (unit) {
                case 'k':
                    g_threshold = KB(n);
                    break;
                case 'm':
                    g_threshold = MB(n);
                    break;
                case 'g':
                    g_threshold = GB(n);
                    break;
                case 't':
                    g_threshold = TB(n);
                    break;
                default:
                    g_threshold = n;
                    break;
            }
        }


        if (vm.count("facility")) {
            std::string val = vm["facility"].as<std::string>();
            for (size_t i = 0; i < sizeof(facilitynames) / sizeof(CODE); ++i) {
                if (strncmp(facilitynames[i].c_name, val.c_str(),
                        strlen(facilitynames[i].c_name)) == 0) {
                    g_syslog_facility = facilitynames[i].c_val;
                    g_syslog_facility_name = facilitynames[i].c_name;
                    break;
                }
            }
        }

        if (vm.count("severity")) {
            std::string val = vm["severity"].as<std::string>();
            for (size_t i = 0; i < sizeof(prioritynames) / sizeof(CODE); ++i) {
                if (strncmp(prioritynames[i].c_name, val.c_str(),
                        strlen(prioritynames[i].c_name)) == 0) {
                    g_syslog_severity = prioritynames[i].c_val;
                    g_syslog_severity_name = prioritynames[i].c_name;
                    break;
                }
            }
        }


    } catch(...) {
        std::cout << desc << std::endl;
        exit(255);
    }
}

void InitGoogleLogging(const char* program)
{
    FLAGS_stderrthreshold = log_stderrthreshold;
    FLAGS_logbufsecs = log_logbufsecs;
    FLAGS_minloglevel = log_minloglevel;
    FLAGS_stop_logging_if_full_disk = log_stop_logging_if_full_disk;
    FLAGS_max_log_size = log_max_log_size;
    FLAGS_v = g_log_v;

    google::InitGoogleLogging(program);

    namespace bfs = boost::filesystem;
    bfs::path logPath = bfs::path(bfs::current_path());
    logPath /= log_folder;
    if (!bfs::exists(logPath)) {
        bfs::create_directory(logPath);
    }

    bfs::path info = logPath;
    info /= "info_";
    bfs::path warning = logPath;
    warning /= "warning_";
    bfs::path error = logPath;
    error /= "error_";
    google::SetLogDestination(google::INFO, info.c_str());
    google::SetLogDestination(google::WARNING, warning.c_str());
    google::SetLogDestination(google::ERROR, error.c_str());
}

int main(int argc, char *argv[])
{
    signal(SIGPIPE, CtrlHandler);
    signal(SIGTERM, CtrlHandler);
    signal(SIGINT, CtrlHandler);
    signal(SIGHUP, CtrlHandler);
    signal(SIGUSR1, CtrlHandler);
    signal(SIGUSR2, CtrlHandler);

    parse_options(argc, argv);
    InitGoogleLogging(argv[0]);

    LOG(INFO) << "========= "
        << PROGRAM_NAME " is starting up, version: "
        << MAJOR_VERSION << "." << MINOR_VERSION << " build " << BUILD_NO
        << " =========";
    LOG(INFO) << "Report syslog facility configured as "
        << g_syslog_facility_name;
    LOG(INFO) << "Report syslog severity configured as "
        << g_syslog_severity_name;
    LOG(INFO) << "Report threshold configured as " << g_threshold;

    CtSessManager manager(g_threshold, g_syslog_facility, g_syslog_severity);
    g_ct_sess_manager = &manager;
    manager.Run();

    return 0;
}
