#include "CtSessManager.h"
#include <fcntl.h>
#include <chrono>
#include <thread>
#include <syslog.h>
#include <arpa/inet.h>
#include <glog/logging.h>
#include "common.h"


// -------------------------------------------------------------
bool CtSession::operator==(const CtSession& other) const
{
    return src == other.src && dst == other.dst &&
        sport == other.sport && dport == other.dport;
}

bool CtSessPtrEqual::operator()(const CtSession* s1, const CtSession* s2) const
{
    return *s1 == *s2;
}

// -------------------------------------------------------------
// nfct message handler
static int CheckNfConn(enum nf_conntrack_msg_type type,
    struct nf_conntrack *ct, void *data)
{
    CtSessManager *manager = (CtSessManager *)data;
    manager->Enqueue(type, ct);
    return NFNL_CB_CONTINUE;
}

// -------------------------------------------------------------------
CtSessManager::CtSessManager(uint64_t thres, int facility, int severity)
    : m_running(false), m_threshold(thres)
    , m_facility(facility), m_severity(severity)
    , m_sessQueue(32)
{
    m_statNfctHandle = nullptr;

    m_evtNfctHandle = nullptr;
    m_evtNfctFd = -1;
}

CtSessManager::~CtSessManager()
{
}

void CtSessManager::Stop()
{
    if (!m_running)
        return ;

    LOG(INFO) << "It's time to stop " PROGRAM_NAME;
    m_running = false;
    m_queueCondition.notify_one();
}

void CtSessManager::Run()
{
    const std::chrono::nanoseconds STAT_INTERVAL(100);

    try {
        m_running = true;
        m_processThr.reset(
            new std::thread(std::bind(&CtSessManager::Process, this)));

        SetupListen();
        PrepareStat();

        auto start = std::chrono::high_resolution_clock::now();
        while (m_running) {
            bool idle = false;
            ListenEvents(idle);

            auto now = std::chrono::high_resolution_clock::now();
            if (now - start > STAT_INTERVAL) {
                start = now;
                GetStat();
            }
        }
    } catch (std::exception& e) {
        // ... We already know the bad news
    } catch (...) {
        LOG(ERROR) << "Unknown error.";
    }

    TeardownStat();
    TeardownListen();

    m_running = false;
    m_queueCondition.notify_one();
    m_processThr->join();
    m_processThr.reset();

    LOG(INFO) << PROGRAM_NAME " is about to exit";
}

void CtSessManager::Enqueue(nf_conntrack_msg_type type, const nf_conntrack *ct)
{
    uint64_t bytes = nfct_get_attr_u64(ct, ATTR_ORIG_COUNTER_BYTES);
    bytes += nfct_get_attr_u64(ct, ATTR_REPL_COUNTER_BYTES);
    if (bytes == 0) return;

    CtSession key;
    key.src = nfct_get_attr_u32(ct, ATTR_IPV4_SRC);
    key.dst = nfct_get_attr_u32(ct, ATTR_IPV4_DST);
    key.sport = nfct_get_attr_u16(ct, ATTR_PORT_SRC);
    key.dport = nfct_get_attr_u16(ct, ATTR_PORT_DST);
    key.proto = nfct_get_attr_u8(ct, ATTR_L4PROTO);
    key.bytes = bytes;
    key.type = type;

    {
        std::lock_guard<std::mutex> guard(m_queueMutex);
        if (m_sessQueue.full()) {
            LOG(ERROR) << "Failed to enqueue new session, slow consumer";
            return ;
        }
        m_sessQueue.push_front(key);
    }
    m_queueCondition.notify_one();
}

void CtSessManager::Process()
{
    while (m_running) {
        CtSession pending;
        {
            std::unique_lock<std::mutex> lock(m_queueMutex);
            m_queueCondition.wait(lock,
                [this]{ return !m_sessQueue.empty() || !m_running; });
            if (!m_running)
                break;

            pending = m_sessQueue.back();
            m_sessQueue.pop_back();
        }

        CtSession *sess = nullptr;
        if (m_sessions.count(&pending)) {
            sess = *m_sessions.find(&pending);
            sess->proto = pending.proto;
            sess->type = pending.type;
            sess->bytes += pending.bytes;
        } else {
            sess = m_pool.construct();
            *sess = pending;
            m_sessions.insert(sess);
        }

        if (VLOG_IS_ON(VLOG_DEBUG))
            VLOG(VLOG_DEBUG) << "Processed"
                << (pending.type == NFCT_T_DESTROY ? " destroyed" : "")
                << " session "
                << IPv4Str(sess->src) << ":" << sess->sport << " <-> "
                << IPv4Str(sess->dst) << ":" << sess->dport << ", protocol: "
                << ProtoStr(sess->proto) << ", bytes " << sess->bytes;

        if (ShouldReport(sess, pending.type == NFCT_T_DESTROY))
        {
            SendSyslog(sess);
            if (pending.type == NFCT_T_DESTROY) {
                m_sessions.erase(sess);
                m_pool.destroy(sess);
            } else {
                sess->bytes = 0;
            }
        }
    }

    LOG(INFO) << "Session queue consumer thread exited";
}

void CtSessManager::PrepareStat()
{
    m_statNfctHandle = nfct_open(NFNL_SUBSYS_CTNETLINK, 0);
    if (!m_statNfctHandle) {
        LOG(ERROR) << "Open dump CTNETLINK subsystem faild, " << strerror(errno);
        THROW_EXCEPTION(OpenCTNetlink, strerror(errno));
    }

    int ret = nfct_callback_register(m_statNfctHandle,
        NFCT_T_ALL, CheckNfConn, this);
    if (ret == -1) {
        LOG(ERROR) << "Register nfct dump callback faild, " << strerror(errno);
        THROW_EXCEPTION(RegisterCallback, strerror(errno));
    }
}

void CtSessManager::GetStat()
{
    int family = AF_INET;
    int ret = nfct_query(m_statNfctHandle, NFCT_Q_DUMP_RESET, &family);
    if (-1 == ret) {
        LOG(ERROR) << "Query kernel faild, " << strerror(errno);
        THROW_EXCEPTION(DumpQuery, strerror(errno));
    }

    if (VLOG_IS_ON(VLOG_TRACE))
        VLOG(VLOG_TRACE) << "Query ctnetlink done";
}

void CtSessManager::TeardownStat()
{
    if (m_statNfctHandle) {
        nfct_callback_unregister(m_statNfctHandle);
        // Don't care if close nfct handler failed, process is exiting
        nfct_close(m_statNfctHandle);
        m_statNfctHandle = nullptr;
    }
}

void CtSessManager::SetupListen()
{
    m_evtNfctHandle = nfct_open(NFNL_SUBSYS_CTNETLINK, NFCT_ALL_CT_GROUPS);
    if (!m_evtNfctHandle) {
        LOG(ERROR) << "Open event CTNETLINK subsystem faild, "
            << strerror(errno);
        THROW_EXCEPTION(OpenCTNetlink, strerror(errno));
    }

    int ret = nfct_callback_register(m_evtNfctHandle,
        NFCT_T_DESTROY, CheckNfConn, this);
    if (-1 == ret) {
        LOG(ERROR) << "Register nfct event callback faild, " << strerror(errno);
        THROW_EXCEPTION(RegisterCallback, strerror(errno));
    }

    m_evtNfctFd = nfct_fd(m_evtNfctHandle);
    int flags = fcntl(m_evtNfctFd, F_GETFL, 0);
    fcntl(m_evtNfctFd, F_SETFL, flags | O_NONBLOCK);
}

void CtSessManager::ListenEvents(bool &idle)
{
    fd_set rfds;
    FD_ZERO(&rfds);
    FD_SET(m_evtNfctFd, &rfds);

    timeval tv{0, 1000};
    int retVal = select(m_evtNfctFd + 1, &rfds, nullptr, nullptr, &tv);
    if (retVal == 0) {
        idle = true;

        if (VLOG_IS_ON(VLOG_TRACE))
            VLOG(VLOG_TRACE) << "No events, going idle";
    } else if (FD_ISSET(m_evtNfctFd, &rfds)) {
        int ret = nfct_catch(m_evtNfctHandle);
        if (ret < 0 && errno != EAGAIN) {
            LOG(ERROR) << "Catch kernel event faild, " << strerror(errno);
            THROW_EXCEPTION(CatchCTEvent, strerror(errno));
        }

        if (VLOG_IS_ON(VLOG_TRACE))
            VLOG(VLOG_TRACE) << "Catch events succeed";
    } else {
        LOG(ERROR) << "Wait CT event failed, " << strerror(errno);
        THROW_EXCEPTION(CatchCTEvent, strerror(errno));
    }
}

void CtSessManager::TeardownListen()
{
    if (m_evtNfctHandle) {
        nfct_callback_unregister(m_evtNfctHandle);
        // Don't care if close nfct handler failed, process is exiting
        nfct_close(m_evtNfctHandle);
        m_evtNfctHandle = nullptr;
    }
}

bool CtSessManager::ShouldReport(const CtSession* sess, bool destroyed)
{
    return sess->bytes > m_threshold || (destroyed && sess->bytes > 0);
}

void CtSessManager::SendSyslog(const CtSession* sess)
{
    try {
        char log[256];
        int n = snprintf(log, sizeof(log), "%d,%s,%s,%u,%u,%lu",
                sess->proto,
                IPv4Str(sess->src).c_str(),
                IPv4Str(sess->dst).c_str(),
                sess->sport,
                sess->dport,
                sess->bytes);
        if (n >= int(sizeof(log))) {
            LOG(ERROR) << "Give up current syslog sending,"
                " due to too many characters for single log, n " << n;
            return ;
        }

        if (VLOG_IS_ON(VLOG_NOTICE)) {
            VLOG(VLOG_NOTICE) << log;
        }

        syslog(m_facility | m_severity, "%s", log);
    } catch (EXCEPTION_NAME(InvalidIPAddr) e) {
        // ... do nothing
    }
}

std::string CtSessManager::IPv4Str(uint32_t ip)
{
    char ipStr[16];
    if (!inet_ntop(AF_INET, &ip, ipStr, sizeof(ipStr))) {
        LOG(ERROR) << "Convert IP to string failed, " << strerror(errno);
        THROW_EXCEPTION(InvalidIPAddr, strerror(errno));
    }
    return ipStr;
}

std::string CtSessManager::ProtoStr(uint8_t proto)
{
    switch (proto) {
        case IPPROTO_TCP:
            return "tcp";
        case IPPROTO_UDP:
            return "udp";
        default:
            return "unknown";
    }
}
