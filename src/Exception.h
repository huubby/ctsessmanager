#ifndef __EXCEPTION_H__
#define __EXCEPTION_H__

#include <exception>
#include <string>

#define DEFINE_EXCEPTION(className) \
class C##className##Exception : public std::exception {\
public:\
    C##className##Exception(const char* _msg){ \
        m_Msg = _msg;\
    }\
    const char* what() const throw(){\
        return m_Msg.c_str();\
    }\
    ~C##className##Exception() throw () {}\
private:\
    std::string m_Msg;\
};

#define THROW_EXCEPTION(className, Message) \
    throw C##className##Exception(Message);

#define EXCEPTION_NAME(className)	C##className##Exception

#define CATCH_EXCEPTION(className) \
    catch (C##className##Exception)


#endif // __EXCEPTION_H__
