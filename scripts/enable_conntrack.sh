#!/bin/bash

lsmod | grep conntrack
if [ $? != 0 ]; then
    modprobe ip_conntrack
fi

if [ $? != 0 ]; then
    echo Failed to enable ip_conntrack kernel module
    exit 255
fi

echo 209715200 > /proc/sys/net/core/rmem_default
echo 419430400 > /proc/sys/net/core/rmem_max

echo 1 > /proc/sys/net/netfilter/nf_conntrack_acct
