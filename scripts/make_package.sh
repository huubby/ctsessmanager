#!/bin/bash

make clean && make release
[ $? == 0 ] || exit 255

[ -d package ] && rm -rf package/
[ -d package/.libs ] || mkdir -p package/.libs

cp _build/csm scripts/setup_syslog package/ && \
cp /usr/lib/x86_64-linux-gnu/libglog.so.0 \
    /usr/lib/x86_64-linux-gnu/libnetfilter_conntrack.so.3 \
    /usr/lib/x86_64-linux-gnu/libboost_program_options.so.1.*.0 \
    /usr/lib/x86_64-linux-gnu/libboost_system.so.1.*.0 \
    /usr/lib/x86_64-linux-gnu/libboost_filesystem.so.1.*.0 \
    /usr/lib/x86_64-linux-gnu/libgflags.so.2 \
    /usr/lib/x86_64-linux-gnu/libnfnetlink.so.0 \
    /usr/lib/x86_64-linux-gnu/libmnl.so.0 \
    /usr/lib/x86_64-linux-gnu/libunwind.so.8 \
    package/.libs/

VERSION=`git log -n 1 | grep -oP '\d+\.\d+\.\d+'`
[ "x$VERSION" == "x" ] && echo "Failed to get version number." && exit 255
tar -cjf csm-v$VERSION.tar.bz2 package/
